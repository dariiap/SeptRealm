//
//  ViewController.swift
//  SeptRealm
//
//  Created by Dariia Pavlovska on 04.09.2022.
//

import UIKit
import RealmSwift

class ViewController: UIViewController {

    let tableView = UITableView()
    var persons: [Person]?
    let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(
            title: "Add New",
            style: .done,
            target: self,
            action: #selector(addPerson))
        setupSubviews()
        
    }

    private func setupSubviews() {
        title = "Realm Test"
        view.addSubview(tableView)
        tableView.frame = view.bounds
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        fetchPersons()
    }
    
    @objc func addPerson() {
        let alert = UIAlertController(title: "Add Person", message: "new name", preferredStyle: .alert)
        alert.addTextField()

        let submitButton = UIAlertAction(title: "Add", style: .default) { (action) in

            let nameTextField = alert.textFields![0]

            //creating new person
            
            let newPerson = Person()
            newPerson.name = nameTextField.text ?? ""
            
            //begin write
            self.realm.beginWrite()
            //adding new data
            self.realm.add(newPerson)

            //saving
            do {
                try self.realm.commitWrite()
            } catch {
                print("Smth wrong with adding new person")
            }

            self.fetchPersons()
        }

        alert.addAction(submitButton)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func fetchPersons() {
        let people = realm.objects(Person.self)
        persons = Array(people)
        tableView.reloadData()
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = persons?[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        persons?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let alert = UIAlertController(title: "Edit Person", message: "Edit name", preferredStyle: .alert)
        alert.addTextField()
        
        let textField = alert.textFields![0]
        let name = self.persons?[indexPath.row].name ?? "default"
        textField.text = name
        
        let saveButton = UIAlertAction(title: "Save", style: .default) { (action) in
            let textField = alert.textFields![0]
            
            let object = self.realm.objects(Person.self).where {
                $0.name == name
            }.first!
            
            try? self.realm.write {
                // Change the name of the person
                object.name = textField.text ?? "Default"
            }
            
            // Fetching updated data
            self.fetchPersons()
        }
        
        alert.addAction(saveButton)
        self.present(alert, animated: true, completion: nil)
    }
    
    internal func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
      if editingStyle == .delete {
          
          guard let personToRemove = self.persons?[indexPath.row] else { return }
          
          // Deleting
          realm.beginWrite()
          realm.delete(personToRemove)
          
          //saving
          do {
              try self.realm.commitWrite()
          } catch {
              print("Smth wrong with deleting person")
          }
          
          // Fetching updated data
          self.fetchPersons()
      }
    }
}

class Person: Object {
    @objc dynamic var name: String = ""
    @objc dynamic var age: Int = 16
}

